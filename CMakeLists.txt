# This file is part of the Flowee project
# Copyright (C) 2019 Tom Zander <tomz@freedommail.ch>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

cmake_minimum_required (VERSION 3.7)
project (floweejs)

set(COPYRIGHT_YEAR 2019)
set(CLIENT_VERSION_MAJOR ${COPYRIGHT_YEAR})
set(CLIENT_VERSION_MINOR 01)
set(CLIENT_VERSION_REVISION 1)

# ------------------------------------------------------------------------------------

if (NOT CMAKE_BUILD_TYPE)
    set(CMAKE_BUILD_TYPE RELWITHDEBINFO)
endif ()
message("build type: ${CMAKE_BUILD_TYPE}")
if (CMAKE_BUILD_TYPE MATCHES "Release")
    set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} -DBCH_NO_DEBUG_OUTPUT")
else()
    set(CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG} -DBCH_LOGCONTEXT")
endif ()

set(CMAKE_CXX_STANDARD 11)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} ${CMAKE_SOURCE_DIR}/external/cmake)

message(STATUS "-=-_-=- Building FloweeJS ${CLIENT_VERSION_MAJOR}-${CLIENT_VERSION_MINOR}.${CLIENT_VERSION_REVISION}")

find_package(OpenSSL REQUIRED)
message ("Using OpenSSL ${OPENSSL_VERSION}")

find_package(Boost 1.56.0
    REQUIRED
    filesystem thread
)

# Include N-API wrappers
execute_process(COMMAND node -p "require('node-addon-api').include"
        WORKING_DIRECTORY ${CMAKE_SOURCE_DIR}
        OUTPUT_VARIABLE NODE_ADDON_API_DIR
        )
string(REPLACE "\n" "" NODE_ADDON_API_DIR "${NODE_ADDON_API_DIR}")
string(REPLACE "\"" "" NODE_ADDON_API_DIR "${NODE_ADDON_API_DIR}")


# create config file
include(CheckSymbolExists)
find_path (HAVE_BYTESWAP_H byteswap.h)
check_symbol_exists(bswap_16 "byteswap.h" HAVE_DECL_BSWAP_16)
check_symbol_exists(bswap_32 "byteswap.h" HAVE_DECL_BSWAP_32)
check_symbol_exists(bswap_64 "byteswap.h" HAVE_DECL_BSWAP_64)
find_path (HAVE_ENDIAN_H endian.h)
find_path (HAVE_SYS_ENDIAN_H sys/endian.h)
check_symbol_exists(le16toh "endian.h" HAVE_DECL_LE16TOH)
check_symbol_exists(be16toh "endian.h" HAVE_DECL_BE16TOH)
check_symbol_exists(be32toh "endian.h" HAVE_DECL_BE32TOH)
check_symbol_exists(be64toh "endian.h" HAVE_DECL_BE64TOH)
check_symbol_exists(htobe16 "endian.h" HAVE_DECL_HTOBE16)
check_symbol_exists(htobe32 "endian.h" HAVE_DECL_HTOBE32)
check_symbol_exists(htobe64 "endian.h" HAVE_DECL_HTOBE64)
check_symbol_exists(htole16 "endian.h" HAVE_DECL_HTOLE16)
check_symbol_exists(htole32 "endian.h" HAVE_DECL_HTOLE32)
check_symbol_exists(htole64 "endian.h" HAVE_DECL_HTOLE64)
check_symbol_exists(le16toh "endian.h" HAVE_DECL_LE16TOH)
check_symbol_exists(le32toh "endian.h" HAVE_DECL_LE32TOH)
check_symbol_exists(le64toh "endian.h" HAVE_DECL_LE64TOH)

set (WORDS_BIGENDIAN ${CMAKE_WORDS_BIGENDIAN})
include (TestBigEndian)
test_big_endian(CMAKE_WORDS_BIGENDIAN)

configure_file (
    "${CMAKE_SOURCE_DIR}/external/flowee-config.h.in"
    ${CMAKE_BINARY_DIR}/config/flowee-config.h
    @ONLY
)
add_definitions(-DHAVE_CONFIG_H)

include_directories(${NODE_ADDON_API_DIR}
    external
    external/utils
    external/crypto
    external/interfaces
    ${CMAKE_JS_INC}
    ${Boost_INCLUDE_DIRS}
    ${CMAKE_BINARY_DIR}
)

# include the flowee source files.
add_subdirectory (external)
add_subdirectory (external/crypto)

# finally, build our plugin
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -fPIC")


add_library(flowee SHARED
    ContextData.cpp
    Engine.cpp
    Flowee.cpp
    Search.cpp
    ${CMAKE_JS_SRC}
)
set_target_properties(flowee PROPERTIES PREFIX "" SUFFIX ".node")
target_link_libraries(flowee
    flowee_lib
    flowee_crypto
    ${Boost_LIBRARIES}
    ${CMAKE_JS_LIB}
)

