#!/bin/bash
cd `dirname $0`

if test -z "$SOURCES_HUB" -o ! -d "$SOURCES_HUB" -o ! -d "$SOURCES_HUB/hub"; then
    echo -e "ERROR: Please export SOURCES_HUB to the absolute path of thehub repo\n"
    exit 1
fi

sources="
    config/flowee-config.h.in
    interfaces/NetworkEnums.h
    interfaces/APIProtocol.h
    apputils/Blockchain.cpp
    apputils/Blockchain.h
    apputils/Blockchain_p.h
    networkmanager/NetworkConnection.cpp
    networkmanager/NetworkConnection.h
    networkmanager/NetworkEndPoint.h
    networkmanager/NetworkException.cpp
    networkmanager/NetworkException.h
    networkmanager/NetworkManager.cpp
    networkmanager/NetworkManager.h
    networkmanager/NetworkManager_p.h
    networkmanager/NetworkQueueFullError.cpp
    networkmanager/NetworkQueueFullError.h
    networkmanager/NetworkServiceBase.cpp
    networkmanager/NetworkServiceBase.h
    networkmanager/NetworkService.cpp
    networkmanager/NetworkService.h
    utils/
    utils/utilstrencodings.h
    utils/utilstrencodings.cpp
    utils/chainparamsbase.h
    utils/chainparamsbase.cpp
    utils/WorkerThreads.h
    utils/WorkerThreads.cpp
    utils/uint256.h
    utils/uint256.cpp
    utils/tinyformat.h
    utils/Logger.cpp
    utils/Logger.h
    utils/utiltime.h
    utils/utiltime.cpp
    utils/LogChannels.cpp
    utils/LogChannels_p.h
    utils/Message.cpp
    utils/Message.h
    utils/prevector.h
    utils/serialize.h
    utils/version.h
    utils/WaitUntilFinishedHelper.h
    utils/WaitUntilFinishedHelper.cpp
    utils/amount.cpp
    utils/amount.h
    utils/cashaddr.cpp
    utils/cashaddr.h
    utils/base58.cpp
    utils/base58.h
    utils/hash.h
    utils/hash.cpp

    utils/streaming/
    utils/streaming/ConstBuffer.h
    utils/streaming/ConstBuffer.cpp
    utils/streaming/MessageBuilder.h
    utils/streaming/MessageBuilder_p.h
    utils/streaming/MessageBuilder.cpp
    utils/streaming/MessageParser.h
    utils/streaming/MessageParser.cpp
    utils/streaming/BufferPool.h
    utils/streaming/BufferPool.cpp
    utils/streaming/streams.h
    utils/primitives/
    utils/primitives/FastTransaction.h
    utils/primitives/FastTransaction.cpp
    utils/primitives/TxIterator_p.h
    utils/primitives/FastBlock.h
    utils/primitives/FastBlock.cpp
    utils/primitives/script.h
    utils/primitives/script.cpp
    utils/primitives/transaction.h
    utils/primitives/transaction.cpp
    utils/primitives/pubkey_utils.cpp
    utils/primitives/pubkey_utils.h
    utils/primitives/block.cpp
    utils/primitives/block.h "

rm -rf external
mkdir external
cat > external/CMakeLists.txt << END
project (flowee_lib)
set(CMAKE_CXX_STANDARD 11)
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -fPIC")
set(CMAKE_CXX_STANDARD_REQUIRED ON)
add_library(flowee_lib STATIC
END
dir=""
for i in $sources
do
    if test ! -f $SOURCES_HUB/libs/$i; then
        dir=$i
        mkdir -p external/$dir
    else
        cp -l $SOURCES_HUB/libs/$i external/$dir
        a=`basename $i`
        echo "    $dir$a" >> external/CMakeLists.txt
    fi
done

echo ")" >> external/CMakeLists.txt

patch -p1 < boost-compatible.diff

cp -r $SOURCES_HUB/libs/crypto external/
