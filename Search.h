/*
 * This file is part of the Flowee project
 * Copyright (C) 2019-2021 Tom Zander <tom@flowee.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef SEARCH_H
#define SEARCH_H

#include "Flowee.h"

#include <Blockchain.h>
#include <napi.h>

class Search : public Blockchain::Search
{
public:
    static Search *create(Napi::Object requestObject);

    // Blockchain::Search interface
    void finished(int unfinishedJobs) override;
    void transactionAdded(const Blockchain::Transaction &transaction, int answerIndex) override;
    void txIdResolved(int jobId, int blockHeight, int offsetInBlock) override;
    void spentOutputResolved(int jobId, int blockHeight, int offsetInBlock) override;
    void addressUsedInOutput(int blockHeight, int offsetInBlock, int outIndex) override;
    void utxoLookup(int jobId, int blockHeight, int offsetInBlock, int outIndex, bool unspent, int64_t amount, Streaming::ConstBuffer outputScript) override;
    void aborted(const Blockchain::ServiceUnavailableException&) override;

    inline Napi::Promise promise(Napi::Env env) {
        return m_promise.promise(env);
    }
    inline void finishupPromise() {
        m_promise.resolve(mainObject());
    }

    // set jobs on mainObject. No validation will be done.
    void setJobs(Napi::Array jobs);
    // append job object to job-list. No validation will be done.
    void addJsJobObject(Napi::Object job);

    void addJsCallbackScheduled();
    void jsCallbackFinished(Napi::Env env);

    void copyResults(Napi::Env env);

    inline Napi::Object mainObject() const {
        return m_me.Value();
    }

    bool useBinaryHashes() const;
    void setUseBinaryHashes(bool useBinaryHashes);

protected:
    Search(Napi::Env env);

private:
    std::unique_ptr<Flowee::Callback> m_onFinished;
    std::unique_ptr<Flowee::Callback> m_onTxAdded;
    std::unique_ptr<Flowee::Callback> m_onTxIdResolved;
    std::unique_ptr<Flowee::Callback> m_onSpentOutputResolved;
    std::unique_ptr<Flowee::Callback> m_onAddressUsedInOutput;
    std::unique_ptr<Flowee::Callback> m_onUtxoLookup;

    Flowee::PromiseCallback m_promise;
    std::unique_ptr<Flowee::Callback> m_promiseFinished;
    Napi::ObjectReference m_me; // javascript version of 'me'.

    std::atomic<int> m_calls;
    bool m_useBinaryHashes = false;
    bool m_done = false;
};

#endif
