/*
 * This file is part of the Flowee project
 * Copyright (C) 2019-2021 Tom Zander <tom@flowee.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef ENGINE_H
#define ENGINE_H

#include <Blockchain.h>
class ContextData;

class Engine : public Blockchain::SearchEngine
{
public:
    friend class ContextData;

    Engine(ContextData *cd);

    void initializeHubConnection(NetworkConnection connection, const std::string &hubVersion) override;
    void initializeIndexerConnection(NetworkConnection connection, const std::set<Blockchain::Service> &services) override;
    void hubDisconnected() override;
    void indexerDisconnected() override;

    // callback from SearchEngine.
    void hubSentMessage(const Message &message) override;
    void indexerSentMessage(const Message &message) override;

    std::string addressForScriptHash(const Streaming::ConstBuffer &buffer) const;

    void setLogLevel(Log::Verbosity verbosity);
    Log::Verbosity logLevel() const;

    enum Net {
        Mainnet,
        Testnet4
    };
    void connect(const std::string &hostname, Net net);
    void connectHub(const std::string &hostname, int port);
    void connectIndexer(const std::string &hostname, int port);

    void setListenToBlockUpdates(bool on);

private:
    bool m_startedHubConnection = false;
    bool m_listenToBlockUpdates = false;
    bool m_startedIndexerConnection = false;

    /// the addresses we subscribed to.
    mutable std::mutex m_subscribeAddressesLock;
    struct SubscribedAddress {
        Streaming::ConstBuffer bitcoinScriptHashed;
        std::string origRequest;
    };
    std::deque<SubscribedAddress> m_subscribedAddresses;
    void subscribe(const SubscribedAddress &ad);
    bool unsubscribe(Streaming::ConstBuffer addressHash);

    Log::Verbosity m_verbosity;

    ContextData *m_cd;
};

#endif
