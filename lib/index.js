/*
 * This file is part of the Flowee project
 * Copyright (C) 2019-2021 Tom Zander <tom@flowee.org>
 * Copyright (C) 2020 James Zuccon <zuccon@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
const _floweeGlobal = require('bindings')('flowee')
const BlockchainService = require('./service.blockchain');
const LiveTransactionsService = require('./service.livetransactions');

const Message = require('./message');

/**
 * Flowee
 * @example
 * const Flowee = require('FloweeJS');
 *
 * async function main() {
 *   // Connect
 *   let flowee = new Flowee();
 *   await flowee.connect();
 * }
 *
 * main();
 */
class Flowee {
  constructor() {
    this.network = _floweeGlobal

    // Create Services
    this.services = {};
    this.services.Blockchain = new BlockchainService(this.network)
    this.services.LiveTransactions = new LiveTransactionsService(this.network)

    // methods to forward to make it nice and easy to use
    this.getBlockCount = this.services.Blockchain.getBlockCount;
    this.getBlock = this.services.Blockchain.getBlock;
    this.getBlockChainInfo = this.services.Blockchain.getBlockChainInfo;
    this.getBestBlockHash = this.services.Blockchain.getBestBlockHash;
    this.getRawTransaction = this.services.Blockchain.getRawTransaction;
    this.getBlockHeader = this.services.Blockchain.getBlockHeader;
    this.getTransaction = this.services.Blockchain.getTransaction;

    this.getLiveTransaction = this.services.LiveTransactions.getLiveTransaction;
    this.sendTransaction = this.services.LiveTransactions.sendTransaction;
    this.isUnspent = this.services.LiveTransactions.isUnspent;
    this.getUnspentOutput = this.services.LiveTransactions.getUnspentOutput;
    this.subscribeToAddress = this.network.subscribeToAddress


    this.connect = this.network.connect;
    this.connectHub = this.network.connectHub;
    this.connectIndexer = this.network.connectIndexer;
    this.sendTransaction = this.network.sendTransaction;
    this.search = this.network.search
    this.stop = this.network.stop

    // enums
    this.Job = this.network.Job
    this.IncludeOutputAmounts = this.network.IncludeOutputAmounts
    this.IncludeOffsetInBlock = this.network.IncludeOffsetInBlock;
    this.IncludeInputs = this.network.IncludeInputs;
    this.IncludeTxid = this.network.IncludeTxid;
    this.IncludeFullTxData = this.network.IncludeFullTxData;
    this.IncludeOutputs = this.network.IncludeOutputs;
    this.IncludeOutputScripts = this.network.IncludeOutputScripts;
    this.IncludeOutputAddresses = this.network.IncludeOutputAddresses;
    this.IncludeOutputScriptHash = this.network.IncludeOutputScriptHash;
    this.IncludeTxFees = this.network.IncludeTxFees;
    this.InfoLevel = this.network.InfoLevel;
    this.WarningLevel = this.network.WarningLevel;
    this.QuietLevel = this.network.QuietLevel;
    this.SilentLevel = this.network.SilentLevel;
  }
}

module.exports = Flowee
