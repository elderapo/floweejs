/*
 * This file is part of the Flowee project
 * Copyright (C) 2019-2020 Tom Zander <tomz@freedommail.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * Flowee Message Packet
 * @param {object} header Header Object containing enumerated values
 * @param {object} body Body Object containing enumerated values
 * @example
 * let m = new Message(1, 2);
 * m.body[7] = 1;
 */
class Message {
  constructor(serviceId, messageId) {
    this.header = {};
    if (typeof serviceId === 'number')
        this.header["serviceId"] = serviceId;
    if (typeof messageId === 'number')
        this.header["messageId"] = messageId;
    this.body = {};
  }
}

module.exports = Message;
