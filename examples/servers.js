const FloweeServices = require('../');
var Flowee = new FloweeServices();

// some fun logging callbacks.
Flowee.network.onConnectHub = function(version) {
    console.log("The Hub reported version: " + version)
}

// some fun logging callbacks.
Flowee.network.onConnectIndexer = function(services) {
    console.log("Indexer supports services: " + services)
}

// connect to the default Flowee server.
Flowee.connect().then(function() {
    console.log("connect completed");
    process.exit();
})

