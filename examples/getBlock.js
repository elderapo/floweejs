const Flowee = require('../');

async function main() {
    let flowee = new Flowee();
    await flowee.connect();

    let block = 500000;
    if (process.argv.length > 2)
        block = process.argv[2];

    let answer = await flowee.getBlock(block);
    console.log("Get block("+ block + ") returned:");
    delete answer.blockHash; // the unchanged binary version
    console.log(answer);
    process.exit();
}

main();
