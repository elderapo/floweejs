const FloweeServices = require('../');
var Flowee = new FloweeServices();

// some fun logging callbacks.
Flowee.network.onConnectHub = function(version) {
    console.log("The Hub reported version: " + version)
}

Flowee.network.onAddressChanged = function(change) {
    console.log("got change: " + change.amount / 100000000 + " BCH");
    console.log(change);
}

Flowee.network.onChainChanged = function(change) {
    console.log("Chain changed " + change.blockHeight);
    console.log(change);
}

// connect to the default Flowee server.
Flowee.connect();

for (arg in process.argv) {
    if (arg > 1) { // nodejs, skip first two args.
        Flowee.subscribeToAddress(process.argv[arg]);
    }
}
