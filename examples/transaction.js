const FloweeServices = require('../');
var Flowee = new FloweeServices();

/**
 * This example creates a search that does two steps before
 * finishing.
 * The first is a FetchTx (based on txid).
 * And when that transaction has been fetched from a Flowee Hub, the
 * onTxAdded method is called with the transaction data.
 * At this point we create a new job that fetches the block-header.
 * On completion we can then print details from both the transaction and the header
 * from our search-result object.
 */

if (process.argv.length <= 2) {
    console.log("Fetch transaction\nUsage node transaction.js [txid]");
    console.log("Example txid: 2c0b2784bae08449e651f3718029cb09a12eb00d5c3a6adc326ba31819892880\n");
    process.exit();
}

function detailTx(txid) {
    return Flowee.search({
        jobs: [{
            value: txid,
            type: Flowee.Job.FetchTx,
            txFilter: [ Flowee.IncludeInputs, Flowee.IncludeOutputs , Flowee.IncludeTxFees]
        }],

        onTxAdded: function(transaction) {
            this.addJob({
                type: Flowee.Job.FetchBlockHeader,
                value: transaction.blockHeight
            });
        }
    });
}

Flowee.connect().then(async function() {
    try {
        for (arg in process.argv) {
            if (arg > 1) { // nodejs, skip first two args.
                let result = await detailTx(process.argv[arg])
                const tx = result.transactions[0];
                console.log("Fetched tx " + tx.txid);
                console.log("{inputs :[");
                for (let id in tx.inputs) {
                    const input = tx.inputs[id];
                    console.log("  {");
                    if (tx.isCoinbase) {
                        console.log("    coinbase");
                    } else {
                        console.log("    txid: " + input.previousTxid);
                        console.log("    vout: " + input.outputIndex);
                    }
                    console.log("    script: " + input.script);
                }
                console.log("  }");
                console.log("]");
                console.log("outputs: [");
                for (let id in tx.outputs) {
                    const output = tx.outputs[id];
                    console.log("  {");
                    let s = "" + output.amount;
                    console.log("    amount: " + output.amount / 100000000);
                    console.log("    script: " + output.script);
                    console.log("  }");
                }
                console.log("  fees: " + tx.fees);

                const header = result[`block${tx.blockHeight}`];
                console.log("  block: " + header.height);
                console.log("  confirmations: " + header.confirmations);
                console.log("  date: " + new Date(header.time * 1000).toDateString());
                console.log("  mined in block: " + header.hash);
                console.log("}");
            }
        }
    } catch (error) {
        console.log("Failure: " + error);
    }
    process.exit();
});
