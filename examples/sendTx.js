const FloweeServices = require('../');
var Flowee = new FloweeServices();

Flowee.onConnectHub = async function() {
    try {
        let result = await Flowee.sendTransaction("01000000012683b60f91bceeb7fa98d8fd908df2cd7e26f8b60066dfaa2876150a477efb26000000006b48304502210086a94b1c5037f188e47625a2d7ae7b1d6dab68fc7c4eb35a0445c1a0162b548b022077e76d36cef0d2599cf2853ebe1477756863433640567cb0ca5dcfc9576528c64121023d2d3f0958cfb9e8129ab64c87314e5437122c499dbb1326956d6181da3d96a3ffffffff021e276bee000000001976a914251bda9cbc8adf7b5403d309d8a8a62018d6463e88ac00ca9a3b000000001976a9148789d39f00ebd1879bd17c0c8ed217a91919bf9a88ac00000000");
        console.log("Sent fine: " + result);
    } catch (error) {
        console.log("Error detected: " + error);
    }
    process.exit();
}

Flowee.connectHub();

