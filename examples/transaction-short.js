const FloweeServices = require('../');
var Flowee = new FloweeServices();

/*
 * This example uses the simplified search.
 * It fetches a transaction by ID and the matching block-header for that transaction.
 *
 * The search() method gets a simple array with "jobs".
 */

if (process.argv.length <= 2) {
    console.log("Fetch transaction\nUsage node transaction.js [txid]");
    console.log("Example address: b39fa6c39b99683ac8f456721b270786c627ecb246700888315991877024b983\n");
    process.exit();
}

function detailTx(txid) {
    return Flowee.search([
        {
            // this job takes a txid and fetches it.
            type: Flowee.Job.FetchTx,
            value: txid,
            txFilter: [ Flowee.IncludeInputs, Flowee.IncludeOutputs ]
        },
        {
            // this job fetches the block header.
            // This job type needs a block height as a value.
            // Instead of passing it in, the Flowee search method will realize it can reuse the
            // blockheight that the previous (the FetchTx above) job returns anyway.
            type: Flowee.Job.FetchBlockHeader
        }
    ]);
}

Flowee.connect().then(async function() {
    try {
        for (arg in process.argv) {
            if (arg > 1) { // nodejs, skip first two args.
                let result = await detailTx(process.argv[arg])
                console.log("Fetched transaction");
                console.log(result.transactions[0]);
                console.log("BlockHeader:");
                console.log(result[`block${result.transactions[0].blockHeight}`]);
            }
        }
    } catch (error) {
        console.log("Failure: " + error);
    }
    process.exit();
});
