const Flowee = require('../');

async function main() {
    let flowee = new Flowee();
    await flowee.connect();

    let answer = await flowee.getBlockCount()
    console.log(" Current blockCount: " + answer);
    process.exit();
}

main();
